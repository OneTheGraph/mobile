import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskSeven {

    public static void main(String[] args){

    }

    public static int[] mergeArrays(int[] a1, int[] a2) {
        int[] result = new int[a1.length + a2.length];
        int aIndex = 0;
        int bIndex = 0;
        int i = 0;

        while (i < result.length) {
            result[i] = a1[aIndex] < a2[bIndex] ? a1[aIndex++] : a2[bIndex++];
            if (aIndex == a1.length) {
                System.arraycopy(a2, bIndex, result, ++i, a2.length - bIndex);
                break;
            }
            if (bIndex == a2.length) {
                System.arraycopy(a1, aIndex, result, ++i, a1.length - aIndex);
                break;
            }
            i++;
        }
        return result;
    }
}