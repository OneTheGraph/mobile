import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskThreeTest {

    @Test
    void flipBit() {
        assertEquals(1, TaskThree.flipBit(0, 1));
        assertEquals(0, TaskThree.flipBit(1, 1));
        assertEquals(2, TaskThree.flipBit(0, 2));
        assertEquals(1024, TaskThree.flipBit(0, 11));
    }
}