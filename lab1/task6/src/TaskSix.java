import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskSix {

    public static void main(String[] args){

    }

    public static BigInteger factorial(int value) {
        return  value <= 1 ? BigInteger.valueOf(1) : BigInteger.valueOf(value).multiply(factorial(value - 1));
    }
}