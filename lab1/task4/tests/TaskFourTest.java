import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskFourTest {

    @Test
    void isPowerOfTwo() {
        assertTrue(TaskFour.isPowerOfTwo(2));
        assertTrue(TaskFour.isPowerOfTwo(4));
        assertTrue(TaskFour.isPowerOfTwo(8));
        assertTrue(TaskFour.isPowerOfTwo(16));

        assertFalse(TaskFour.isPowerOfTwo(17));
        assertFalse(TaskFour.isPowerOfTwo(18));
        assertFalse(TaskFour.isPowerOfTwo(19));
        assertFalse(TaskFour.isPowerOfTwo(20));
    }
}