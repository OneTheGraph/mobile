
public class TaskOne {

    public static void main(String[] args){
        final boolean var_a = true;
        final boolean var_b = false;
        final boolean var_c = true;
        final boolean var_d = false;

        booleanExpression(var_a, var_b, var_c, var_d);
    }

    public static boolean booleanExpression(boolean a, boolean b, boolean c, boolean d){
        return (c&d) ^ (b&d) ^ (b&c) ^ (b&c&d) ^ (a&d) ^ (a&c) ^ (a&c&d) ^ (a&b) ^ (a&b&d) ^ (a&b&c);
    }
}