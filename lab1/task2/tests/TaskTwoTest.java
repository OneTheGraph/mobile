import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskTwoTest {

    @Test
    void leapYearCount() {
        assertEquals(0, TaskTwo.leapYearCount(1));
        assertEquals(490, TaskTwo.leapYearCount(2020));
        assertEquals(481, TaskTwo.leapYearCount(1986));
        assertEquals(484, TaskTwo.leapYearCount(1999));
    }
}