import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskFive {

    public static void main(String[] args){

    }

    public static boolean isPalindrome(String text){
        String reg_text = text.replaceAll("[^\\da-zA-Zа-яёА-ЯЁ]", "").toLowerCase();
        String reg_text_reverse = new StringBuilder(reg_text).reverse().toString();
        if(reg_text.equals(reg_text_reverse))
            return true;
        else
            return false;
    }
}