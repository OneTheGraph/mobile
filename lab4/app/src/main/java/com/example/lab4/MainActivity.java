package com.example.lab4;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    ListView lvMain;

    String[] synth_names = new String[] {"Roland TB-303", "Moog Minimoog", "Roland SE-02", "Roland SH-101", "Sequential Circuits Pro One"};

    HashMap<Integer, String> text_synth = new HashMap<>();
    HashMap<Integer, Integer> image_synth = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createActionBar();
        createTextSynth();
        createImageSynth();
        createListView();
    }

    void createListView(){
        lvMain = (ListView) findViewById(R.id.list_viewMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, synth_names);

        lvMain.setAdapter(adapter);
        lvMain.setClickable(true);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d("My log:", "itemClick: position = " + position + ", id = "
                        + id);
                Intent intent = new Intent(MainActivity.this, DisplaySynthActivity.class);

                intent.putExtra("synth_name", synth_names[position]);
                intent.putExtra("synth_text", text_synth.get(position));
                intent.putExtra("synth_image", image_synth.get(position));

                startActivity(intent);
            }
        });
    }

    void createTextSynth(){
        text_synth.put(0, "Недорогой синтезатор с ценником в районе $300, пластиковым серебряным корпусом и ограниченной звуковой палитрой неожиданно полюбился музыкантам, подвинул с царского трона Minimoog и ворвался во многие музыкальные стили, где стал настоящим стандартом для нескольких поколений музыкантов и продюсеров.\n" +
                "\n" +
                "Тем не менее, успех к синтезатору пришел не сразу. Вышедший в 1981 году Roland TB-303 провалился на рынке и считался дешевой поделкой, а сама Roland вскоре решила свернуть производство и выпуск синтезатора. Синтезатор постепенно попал на вторичный рынок, где получил культовый статус и стал настоящей легендой среди музыкантов. Несмотря на ограниченные технические возможности, TB-303 полюбился публике за широкий диапазон воспроизводимых звуков — синтезатор с легкостью создавал объемные, мутные, грязные или яркие, но неизменно сочные и мощные звуки.");
        text_synth.put(1, "Монофонический синтезатор Moog Minimoog, выпущенный Робертом Мугом и Дэвидом ван Куверингом в 1970 году, стал основой всего современного синтезаторного саунда. Музыкантам пришлись по вкусу три осциллятора (слегка нестабильных), легендарный транзисторный лестничный фильтр (24dB/oct), возможность маршрутизации сигнала наушников через секцию фильтра, два простых и производительных генератора огибающей, а также относительно невысокая цена, что, в свою очередь, сделало Minimoog невероятно популярным устройством.\n" +
                "\n" +
                "Несмотря на то, что синтезатор был спроектирован как простой инструмент для создания лид-звуков и эффектов, музыканты по всему миру открыли для себя Minimoog в качестве мощного источника басовых звуков. Глядя на то, как Minimoog популярен у музыкантов в вопросах баса, Moog быстро разработали отдельный басовый синтезатор Moog Taurus.");
        text_synth.put(2, "Если, как и мы, вы в последние время сплетничали о цифровом движке Roland, будьте готовы взять ваши слова обратно. Коллаборация со Studio Electronics привела к созданию SE02, а это аналоговый моносинтезатор с тремя осцилляторами, 16-шаговым секвенсором и внушительным количеством пресетов на борту.");
        text_synth.put(3, "Roland SH-101 вышел во времена, когда на рынке уже вовсю правили программируемые и полифонические синтезаторы, и стал последним моносинтезатором японской компании. Несмотря на изменившиеся предпочтения музыкантов, с 1982 по 1984 год Roland произвела более 50 000 SH-101, чему поспособствовала относительно небольшая цена на устройство — около $350. Как и в случае с Minimoog, SH-101 предназначался для лид-звуков, но популярность в музыкальной среде снискал за счет басового звука, отличавшегося плотностью и сочностью.\n" +
                "\n" +
                "Roland SH-101 оснащался одним осциллятором и одним суб-осциллятором и предлагал три формы звуковой волны: Saw, Pulse и Noise. При этом смена используемой формы осуществлялась не отдельным переключателем, а с помощью специального микшера, с помощью которого музыкант мог настроить количество той или иной звуковой волны в общем миксе.\n" +
                "\n" +
                "Ключевой особенностью Roland SH-101 стал встроенный и простой в использовании секвенсор, который позволял направлять звуки на низкочастотный осциллятор. В дополнение к этому, у пользователя имелась возможность транспонировать любую секвенцию с помощью клавиатуры синтезатора.");
        text_synth.put(4, "Компания Sequential Circuits прежде всего известна своим программируемым синтезатором Prophet-5, вышедшем в 1978 году. Prophet-5 быстро обрел популярность и даже цена в районе $4 000 никого не остановила. Спустя три года после релиза, компания выпустила монофоническую версию синтезатора, получившую название Sequential Circuits Pro One.\n" +
                "\n" +
                "Как и Prophet-5, Sequential Circuits Pro One оснащался двумя осцилляторами и резонансным LP-фильтром 24 dB, основанным на популярном чипе Curtis. Оба осциллятора отличались качественным звуком — низкие частоты звучали мощно и чисто, а высокие отличались яркостью, которая прорывалась даже через самые плотные миксы. В дополнение к этому, Pro One предлагал богатые возможности модуляции и маршрутизации, а также отдельные генераторы ADSR-огибающей для VCA и фильтра.\n" +
                "\n" +
                "Наличие встроенного секвенсора и арпедижатора, вкупе с остальным оснащением, делало Pro One идеальным устройством для создания интересных, необычных и качественно звучащих басовых патчей.");
    }

    void createImageSynth(){
        image_synth.put(0, R.drawable.tb303);
        image_synth.put(1, R.drawable.moog);
        image_synth.put(2, R.drawable.se02);
        image_synth.put(3, R.drawable.sh101);
        image_synth.put(4, R.drawable.proone);
    }

    void createActionBar(){
        ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle("Популярные синтезаторы");
    }
}