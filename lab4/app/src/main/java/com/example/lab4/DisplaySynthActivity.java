package com.example.lab4;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class DisplaySynthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_synth);

        createActionBarSettings();

        TextView infoTextView = (TextView)findViewById(R.id.textViewInfo);
        ImageView infoImageView = (ImageView)findViewById(R.id.imageViewInfo);

        infoTextView.setText(getIntent().getExtras().getString("synth_text"));
        infoImageView.setImageResource(getIntent().getExtras().getInt("synth_image"));
    }

    void createActionBarSettings(){
        ActionBar actionBar = getSupportActionBar();

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle(getIntent().getExtras().getString("synth_name"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}