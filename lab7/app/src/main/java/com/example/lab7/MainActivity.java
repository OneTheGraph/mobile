package com.example.lab7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new BubbleView(this));
    }

    class BubbleView extends View {
        ArrayList<Bubble> bubbleList;
        Random rand;
        Canvas canvas;

        public BubbleView(Context context) {
            super(context);
            bubbleList = new ArrayList<Bubble>();
            canvas = new Canvas();
            rand = new Random();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
            for (Bubble b : bubbleList) {
                b.draw(canvas);
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            int x = (int) event.getX();
            int y = (int) event.getY();
            int size = rand.nextInt(100);
            Paint paint = new Paint();
            paint.setARGB(255, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
            bubbleList.add(new Bubble(x, y, size, paint));
            this.postInvalidate();
            return true;
        }
    }
}
